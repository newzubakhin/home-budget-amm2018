FROM openjdk:8-alpine

RUN apk add --no-cache bash

ENV PROJECT_HOME /opt/home-budget

RUN mkdir -p $PROJECT_HOME/build/libs
RUN mkdir -p $PROJECT_HOME/util

COPY build/libs/* $PROJECT_HOME/build/libs/
COPY util/run-service.sh $PROJECT_HOME/util

WORKDIR $PROJECT_HOME

ENTRYPOINT ["bash", "util/run-service.sh"]
