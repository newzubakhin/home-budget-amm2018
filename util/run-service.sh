#!/bin/bash

set -x -e

mkdir -p /usr/demarcation
mkdir -p /usr/target

JAR_PATTERN="home-budget-*.jar"
JAR_FILE=$(find build/ -name ${JAR_PATTERN} | tail -n1)

if [[ "${JAR_FILE}" = "" ]]
then
    printf "Jar file not found. Please, put home-budget-*.jar file in project root directory.\n"
    exit 1
fi

# run our jar preserving current PID
exec java -jar ${JAR_FILE}
